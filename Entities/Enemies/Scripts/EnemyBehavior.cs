﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EnemyBehavior : MonoBehaviour {

	public GameObject FloatText;
	public int health;
	public int maxHealth;
	public int touchDamage;
	public bool isBoss;

	private bool isBossAlive = true;
	private BoxCollider2D bossExitBlocker;

	private Image healthBar;
	private EnemyAttackBoss enemyAttackBoss;

	void Start() {
		healthBar = transform.FindChild("Canvas").FindChild("HealthBG").FindChild("Health").GetComponent<Image>();
		enemyAttackBoss = GetComponent<EnemyAttackBoss>();

		if(isBoss) {
			Debug.Log("boss is here");
			bossExitBlocker = GameObject.Find("Boss Exit Blocker").GetComponent<BoxCollider2D>();
			bossExitBlocker.enabled = true;
		}

//		Debug.Log("there are " + enemyAttackBoss.CountLegs() + " legs left");
	}

	void OnTriggerEnter2D(Collider2D collider) {
		Weapon weapon = collider.gameObject.GetComponent<Weapon>();
		if(collider.gameObject.tag == "PlayerWeapon") {
			if(isBoss) {
				Debug.Log("player hit the boss");
				if(enemyAttackBoss.CountLegs() > 0) {
					Debug.Log("boss is still invincible");
					CreateFloatText("0").GetComponent<Animator>().SetTrigger("Hit");
				}
				else {
					Debug.Log("boss has no legs and can take damage");
					Debug.Log("Hit by a player weapon");
					Debug.Log("Enemy health is at " + health);
					CreateFloatText(weapon.GetDamage().ToString()).GetComponent<Animator>().SetTrigger("Hit");
					StartCoroutine(Flasher());
					health -= weapon.GetDamage();
					healthBar.fillAmount = (float)health / (float)maxHealth;
					Debug.Log("Enemy health is now at " + health);
					weapon.Hit();
					if(health <= 0) {
						Debug.Log("Enemy health is zero or below. dying. " + health);
						Die();
					}
				}
			}
			else {
				Debug.Log("Hit by a player weapon");
				Debug.Log("Enemy health is at " + health);
				CreateFloatText(weapon.GetDamage().ToString()).GetComponent<Animator>().SetTrigger("Hit");
				StartCoroutine(Flasher());
				health -= weapon.GetDamage();
				healthBar.fillAmount = (float)health / (float)maxHealth;
				Debug.Log("Enemy health is now at " + health);
				weapon.Hit();
				if(health <= 0) {
					Debug.Log("Enemy health is zero or below. dying. " + health);
					Die();
				}
			}
		}
		if(collider.gameObject.tag == "PlayerFriendly") {
			Debug.Log("hit by player friendly");
			Destroy(gameObject);
		}
//		if(collider.gameObject.tag == "Player") {
//			Debug.Log("Hit by a player");
//			Debug.Log("Enemy health is at " + health);
//			CreateFloatText(weapon.GetDamage().ToString()).GetComponent<Animator>().SetTrigger("Hit");
//			StartCoroutine(Flasher());
//			health -= touchDamage;
//			healthBar.fillAmount = (float)health / (float)maxHealth;
//			Debug.Log("Enemy health is now at " + health);
//			if(health <= 0) {
//				Debug.Log("Enemy health is zero or below. dying. " + health);
//				Die();
//			}
//		}
	}

	GameObject CreateFloatText(string text) {
		Debug.Log("Running CreateFloatText");
		GameObject temp = Instantiate(FloatText) as GameObject;
		RectTransform tempRect = temp.GetComponent<RectTransform>();
		temp.transform.SetParent(transform.FindChild("Canvas"));
		tempRect.transform.localPosition = FloatText.transform.localPosition;
		tempRect.transform.localScale = FloatText.transform.localScale;
		tempRect.transform.localRotation = FloatText.transform.localRotation;

		temp.GetComponent<Text>().text = text;
//		temp.GetComponent<Animator>().SetTrigger("Hit");
		Destroy(temp.gameObject, 2);

		return temp;
	}

	public int TouchDamage() {
		Debug.Log("Touch damage is " + touchDamage);
		return touchDamage;
	}

	void Die() {
		Debug.Log("Enemy Died");
		Debug.Log("Enemy being destroyed is " + gameObject);
		if(isBoss) {
			Debug.Log("bye octopus");
			bossExitBlocker.enabled = false;
		}
		Destroy(gameObject);
	}

	IEnumerator Flasher() 
	{
		Debug.Log("Running Flasher");
		for (int i = 0; i < 3; i++)
		{
			Debug.Log("Flash");
			GetComponent<Renderer>().material.color = Color.magenta;
			yield return new WaitForSeconds(.05f);
			GetComponent<Renderer>().material.color = Color.cyan;
			yield return new WaitForSeconds(.05f);
			GetComponent<Renderer>().material.color = Color.yellow;
			yield return new WaitForSeconds(.05f);
			GetComponent<Renderer>().material.color = Color.white;
			yield return new WaitForSeconds(.05f);
		}
	}
}
