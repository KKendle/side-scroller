﻿using UnityEngine;
using System.Collections;

public class EnemyAttack : MonoBehaviour {

	public float shotsPerSecondMin = 2.5f;
	public float shotsPerSecondMax = 5.0f;
	public float weaponSpeed = 5.0f;
	public GameObject weapon;
	public AudioClip fireSound;

	private EnemyMovement enemyMovement;
	private SpriteRenderer spriteRenderer;

	private bool hasAmmo = true;

	void Start() {
		enemyMovement = GetComponent<EnemyMovement>();
	}

	void Update() {
//		float probability = Time.deltaTime * shotsPerSecond;
//		if(Random.value < probability) {
//			Fire(enemyMovement.FacingRight());
//		}
		Fire(enemyMovement.FacingRight());
	}

	void Fire(bool facingRight) {
		if(hasAmmo == true) {
			Debug.Log("shoot");
			hasAmmo = false;
			StartCoroutine(Reload());

			if(facingRight == true) {
				Vector3 startPosition = transform.position + new Vector3(.75f, 0, 0);
				GameObject missile = Instantiate(weapon, startPosition, Quaternion.identity) as GameObject;
				spriteRenderer = missile.GetComponent<SpriteRenderer>();
				missile.GetComponent<Rigidbody2D>().velocity = new Vector3(weaponSpeed, 0, 0);
				spriteRenderer.flipX = true;
				AudioSource.PlayClipAtPoint(fireSound, transform.position);
			}
			if(facingRight == false) {
				Vector3 startPosition = transform.position + new Vector3(-.75f, 0, 0);
				GameObject missile = Instantiate(weapon, startPosition, Quaternion.identity) as GameObject;
				spriteRenderer = missile.GetComponent<SpriteRenderer>();
				missile.GetComponent<Rigidbody2D>().velocity = new Vector3(-weaponSpeed, 0, 0);
				spriteRenderer.flipX = false;
				AudioSource.PlayClipAtPoint(fireSound, transform.position);
			}
		}
	}

	IEnumerator Reload() {
		Debug.Log("reloading");
		float shotsPerSecond = Random.Range(shotsPerSecondMin, shotsPerSecondMax);
		yield return new WaitForSeconds(shotsPerSecond);
		hasAmmo = true;
	}

	void OnTriggerEnter2D(Collider2D collider) {
		if(collider.gameObject.tag == "Player") {
			Debug.Log("weapon hit player");
		}
	}
}
