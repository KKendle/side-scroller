﻿using UnityEngine;
using System.Collections;

public class EnemyMovement : MonoBehaviour {
	
	private float speed;
	private float leftEdge;
	private float rightEdge;

	private bool dirRight = true;
	private float direction;
	private SpriteRenderer spriteRenderer;

	void Start() {
		spriteRenderer = GetComponent<SpriteRenderer>();
		leftEdge = transform.position.x + 2;
		rightEdge = transform.position.x + -2;
		speed = Random.Range(.5f, 1.25f);
		direction = Random.Range(0f, 1f);
		if(direction >= .500001f) {
			dirRight = true;
		}
		if(direction <= .499999f) {
			dirRight = false;
		}
	}

	void Update () {
//		transform.position =new Vector3(Mathf.PingPong(Time.time*2,max-min)+min, transform.position.y, transform.position.z);
		if (dirRight) {
			transform.Translate (Vector2.right * speed * Time.deltaTime);
			spriteRenderer.flipX = true;
		}
		else {
			transform.Translate (-Vector2.right * speed * Time.deltaTime);
			spriteRenderer.flipX = false;
		}

		if(transform.position.x >= leftEdge) {
			dirRight = false;
		}

		if(transform.position.x <= rightEdge) {
			dirRight = true;
		}
	}

	public bool FacingRight() {
		return dirRight;
	}
}
