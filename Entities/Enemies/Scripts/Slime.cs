﻿using UnityEngine;
using System.Collections;

public class Slime : MonoBehaviour {

	public GameObject slime;
//	public GameObject sploosh;
	public AudioClip dripSound;

//	private float dripRate = 3.0f;
	private Animator animator;

	void Start () {
//		animator = GameObject.Find("slime-drip_01").GetComponent<Animator>();
		animator = GetComponent<Animator>();
		StartCoroutine(Drip());
//		Drip();
	}

	void Update () {
//		float dripChance = Random.Range(0.0f, 100.0f);
//		if(dripChance >= 90.0f) {
//			Debug.Log("Creating slime droplet");
//			animator.SetTrigger("Drip");
//		}
//		StartCoroutine(Drip());

	}

	IEnumerator Drip() {
		while(true) {
			animator.SetTrigger("Drip");
			yield return new WaitForSeconds(.5f);
			GameObject droplet = Instantiate(slime, transform.position, Quaternion.identity) as GameObject;
			droplet.transform.parent = gameObject.transform;
	//		Instantiate(sploosh, sploosh.transform.position, Quaternion.identity);
			AudioSource.PlayClipAtPoint(dripSound, transform.position);
			yield return new WaitForSeconds(Random.Range(2.5f, 4.75f));
		}
	}
}
