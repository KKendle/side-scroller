﻿using UnityEngine;
using System.Collections;

public class EnemyAttackBoss : MonoBehaviour {

	public float shotsPerSecondMin = 2.5f;
	public float shotsPerSecondMax = 5.0f;
	public float weaponSpeed = 5.0f;
	public GameObject weapon;
	public Sprite[] sprites;
	public AudioClip fireSound;

	private EnemyMovement enemyMovement;
	private SpriteRenderer spriteRenderer;
	private SpriteRenderer spriteRendererLegs;

	private bool hasAmmo = true;
	private int legCount = 8;
	private int maxLegCount = 8;

	void Start() {
//		enemyMovement = GetComponent<EnemyMovement>();
		spriteRendererLegs = GetComponent<SpriteRenderer>();
	}

	void Update() {
//		float probability = Time.deltaTime * shotsPerSecond;
//		if(Random.value < probability) {
//			Fire(enemyMovement.FacingRight());
//		}
		Fire();
	}

	public int CountLegs() {
		return legCount;
	}

	void Fire() {
		if(hasAmmo == true) {
			Debug.Log("shoot");
			hasAmmo = false;
			StartCoroutine(Reload());
			ThrowLeg();

			Vector3 startPosition = transform.position + new Vector3(-.75f, -.3f, 0);
			GameObject missile = Instantiate(weapon, startPosition, Quaternion.identity) as GameObject;
			spriteRenderer = missile.GetComponent<SpriteRenderer>();
			missile.GetComponent<Rigidbody2D>().velocity = new Vector3(-weaponSpeed, 0, 0);
			AudioSource.PlayClipAtPoint(fireSound, transform.position);
		}
	}

	IEnumerator Reload() {
		Debug.Log("reloading");
		float shotsPerSecond = Random.Range(shotsPerSecondMin, shotsPerSecondMax);
		yield return new WaitForSeconds(shotsPerSecond);
		hasAmmo = true;
	}

	void ThrowLeg() {
		Debug.Log("throwing leg");
		Debug.Log("leg count is: " + legCount);
		if(legCount < 1) {
			legCount = maxLegCount;
			spriteRendererLegs.sprite = sprites[legCount];
		}
		else {
			legCount--;
			spriteRendererLegs.sprite = sprites[legCount];
		}
	}

	void OnTriggerEnter2D(Collider2D collider) {
		if(collider.gameObject.tag == "Player") {
			Debug.Log("weapon hit player");
		}
	}
}
