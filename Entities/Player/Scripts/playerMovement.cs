﻿using UnityEngine;
using System.Collections;

public class playerMovement : MonoBehaviour {

	public float speed;
	public float jumpPower;
	public GameObject potionVial; // actual potion

	private bool attacking = false;
	private float attackTimer = 0f;
	private float attackCoolDown = .3f;
	private float swingRate = 0.5f;
	private bool playerFlipped = false; // which way sprite is facing
	private Potion potion; // script
	private Animator animator;
	private SpriteRenderer playerSprite;

	// jumping or not
	bool grounded;

	// for potion throwing
	private float offsetX = .5f;
	private float offsetXFlipped = -.5f;
	private float offsetY = -.2f;
	public float throwX = 2.5f;
	public float throwXFlipped = -2.5f;
	public float throwY = 2f;
	private float throwPower = 1.6f;

	void Start() {
//		//Get a component reference to the Player's animator component
		animator = GameObject.Find("Scientist").GetComponent<Animator>();
		if (animator == null) {
			Debug.LogError ("Didn't find animator!");
		}
		playerSprite = GetComponent<SpriteRenderer>();

//		attackTrigger.enabled = false;

//		potion = GameObject.Find("potion").GetComponent<Potion>();
//		GameObject addPotion = Instantiate(potionVial, new Vector3(0.504f, 1.52f, -2f), Quaternion.identity) as GameObject;
//		addPotion.transform.parent = GameObject.Find("Scientist").transform;
	}

	void Update () {
		if(Input.GetKeyDown(KeyCode.Space)) {
			InvokeRepeating("Throw", 0.00001f, swingRate);
//			InvokeRepeating("Throw", swingRate, swingRate);
		}
		if(Input.GetKeyUp(KeyCode.Space)) {
			CancelInvoke("Throw");
		}
//		if(Input.GetKeyDown(KeyCode.Space) && !attacking) {
//			attacking = true;
//			attackTimer = attackCoolDown;

//			animator.SetTrigger("Swing");
//			attackTrigger.enabled = true;

//			potion.ThrowPotion();
//			GameObject addPotion = Instantiate(potion, new Vector3(0.504f, 1.52f, -2f), Quaternion.identity) as GameObject;
//			addPotion.transform.parent = GameObject.Find("Scientist").transform;
//		}

		if(Input.GetKey(KeyCode.LeftArrow)) {
			transform.position += Vector3.left * speed * Time.deltaTime;
			if(playerFlipped == false) {
				playerSprite.flipX = true;
				playerFlipped = true;
			}
		}
		else if(Input.GetKey(KeyCode.RightArrow)) {
			transform.position += Vector3.right * speed * Time.deltaTime;
			if(playerFlipped == true) {
				playerSprite.flipX = false;
				playerFlipped = false;
			}
		}

		if(Input.GetKeyDown(KeyCode.UpArrow) && (grounded)) {
			Debug.Log("Up arrow pressed");
			Jump();
		}
	}

	void OnCollisionStay2D(Collision2D collider) {
		CheckIfGrounded();
	}

	void OnCollisionExit2D(Collision2D collider) {
		grounded = false;
	}

	void CheckIfGrounded() {
//		Debug.Log("Checking if grounded");
		RaycastHit2D[] hits;

		//We raycast down 1 pixel from this position to check for a collider
		Vector2 positionToCheck = transform.position;
		hits = Physics2D.RaycastAll (positionToCheck, new Vector2 (0, -1), 0.01f);

		//if a collider was hit, we are grounded
		if (hits.Length > 0) {
			grounded = true;
//			Debug.Log("On the ground");
		}
	}

	void Jump() {
		Rigidbody2D rbody = GetComponent<Rigidbody2D>();
		rbody.AddForce(Vector2.up * jumpPower);
	}

	void Throw() {
		animator.SetTrigger("Throw");
		StartCoroutine(Attack());
	}

	IEnumerator Attack() {
		yield return new WaitForSeconds(.30f);
		if(playerFlipped == false) {
			Vector3 startPosition = transform.position + new Vector3(offsetX, offsetY, 0);
			GameObject weapon = Instantiate(potionVial, startPosition, Quaternion.identity) as GameObject;
			weapon.GetComponent<Rigidbody2D>().velocity = new Vector3(throwX, throwY, 0) * throwPower;
		}
		if(playerFlipped == true) {
			Vector3 startPosition = transform.position + new Vector3(offsetXFlipped, offsetY, 0);
			GameObject weapon = Instantiate(potionVial, startPosition, Quaternion.identity) as GameObject;
			weapon.GetComponent<Rigidbody2D>().velocity = new Vector3(throwXFlipped, throwY, 0) * throwPower;
		}
	}
}