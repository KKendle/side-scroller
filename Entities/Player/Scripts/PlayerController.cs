﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerController : MonoBehaviour {

	private PlayerHealth playerHealth;
	private LevelManager levelManager;

	void Start() {
		playerHealth = GameObject.Find("PlayerHealth").GetComponent<PlayerHealth>();
		if(playerHealth == null) {
			Debug.Log("Player Health not found");
		}

		levelManager = GameObject.Find("Level Manager").GetComponent<LevelManager>();
		if(levelManager == null) {
			Debug.Log("Level Manager not found");
		}
	}

	void OnTriggerEnter2D(Collider2D collider) {
		Weapon weapon = collider.gameObject.GetComponent<Weapon>();
		EnemyBehavior enemyBehavior = collider.gameObject.GetComponent<EnemyBehavior>();
		if(collider.gameObject.tag == "EnemyWeapon") {
			Debug.Log("Player hit by EnemyWeapon");
			Debug.Log("Player health is at " + PlayerHealth.playerHealth);
			StartCoroutine(Flasher());
			playerHealth.Health(weapon.GetDamage());
			weapon.Hit();
			Debug.Log("Player health is now at " + PlayerHealth.playerHealth);
			if(PlayerHealth.playerHealth <= 0) {
				Debug.Log("Player health is zero or below. dying. " + PlayerHealth.playerHealth);
				Die();
			}
		}
		if(collider.gameObject.tag == "Enemy") {
			Debug.Log("Player hit by Enemy");
			Debug.Log("Player health is at " + PlayerHealth.playerHealth);
			StartCoroutine(Flasher());
			playerHealth.Health(enemyBehavior.TouchDamage());
			Debug.Log("Player health is now at " + PlayerHealth.playerHealth);
			if(PlayerHealth.playerHealth <= 0) {
				Debug.Log("Player health is zero or below. dying. " + PlayerHealth.playerHealth);
				Die();
			}
		}
	}

	IEnumerator Flasher() 
	{
		for (int i = 0; i < 3; i++)
		{
			float flashTimer = .05f;
			GetComponent<Renderer>().material.color = Color.magenta;
			yield return new WaitForSeconds(flashTimer);
			GetComponent<Renderer>().material.color = Color.cyan;
			yield return new WaitForSeconds(flashTimer);
			GetComponent<Renderer>().material.color = Color.yellow;
			yield return new WaitForSeconds(flashTimer);
			GetComponent<Renderer>().material.color = Color.white;
			yield return new WaitForSeconds(flashTimer);
		}
	}

	void Die() {
		Debug.Log("Player Died");
//		Destroy(gameObject);
//		LevelManager levelManager = GameObject.Find("LevelManager").GetComponent<LevelManager>();
		levelManager.LoadLevel("Lose");
	}
}
