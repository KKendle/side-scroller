﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerHealth : MonoBehaviour {

	// static means it belongs to class itself, not a created instance of the class
	// meaning, there is only one playerHealth
	public static int playerHealth = 1000;
	private int maxHealth = 1000;
	static PlayerHealth instance = null;

//	private Text healthText;
	private Image healthBar;

	void Start() {
		if (instance != null && instance != this) {
			Destroy(gameObject);
		}
		else {
			instance = this;
			GameObject.DontDestroyOnLoad(gameObject);
//			healthText = GetComponent<Text>();
//			healthText.text = playerHealth.ToString();
		}
		healthBar = transform.FindChild("Canvas").FindChild("HealthBG").FindChild("Health").GetComponent<Image>();
		FillHealthbar();
	}

	void FillHealthbar() {
		Debug.Log("in FillHealthbar");
		healthBar.fillAmount = (float)playerHealth / (float)maxHealth;
		if(healthBar.fillAmount >= maxHealth) {
			healthBar.fillAmount = maxHealth;
		}
	}

	public void Health(int points) {
		playerHealth -= points;
		if(playerHealth <= 0) {
			playerHealth = 0;
		}
		healthBar.fillAmount = (float)playerHealth / (float)maxHealth;
//		healthText.text = playerHealth.ToString();
	}

	public void HealthRestore(int points) {
		Debug.Log("adding " + points + " health");
		playerHealth += points;
		Debug.Log("player new health is: " + playerHealth);
		healthBar.fillAmount = (float)playerHealth / (float)maxHealth;
		if(playerHealth >= maxHealth) {
			Debug.Log("too much health");
			healthBar.fillAmount = maxHealth;
			playerHealth = maxHealth;
		}
//		healthText.text = playerHealth.ToString();
	}

	public static void Reset() {
		Debug.Log("Player health is at " + playerHealth);
		Debug.Log("Resetting player health");
		playerHealth = 1000;
		Debug.Log("Player health now is at " + playerHealth);
	}
}
