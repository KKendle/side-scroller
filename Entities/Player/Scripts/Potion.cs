﻿using UnityEngine;
using System.Collections;

public class Potion : MonoBehaviour {

	public GameObject potionBreak;

	private Animator animator;

	void Start() {
		animator = GetComponent<Animator>();
	}

	void OnTriggerEnter2D(Collider2D collider) {
		if(collider.gameObject.tag == "Enemy") {
			Debug.Log("Potion hit enemy");
			PotionBreak();
			Destroy(gameObject);
		}
		if(collider.gameObject.tag == "Ground") {
			Debug.Log("Potion hit ground");
			PotionBreak();
			Destroy(gameObject);
		}
		if(collider.gameObject.tag == "EnemyWeapon") {
			Debug.Log("Potion hit enemy weapon");
			PotionBreak();
			Destroy(gameObject);
		}
	}

	void PotionBreak() {
		Instantiate(potionBreak, transform.position, Quaternion.identity);
	}
}
