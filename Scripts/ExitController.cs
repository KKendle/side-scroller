﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ExitController : MonoBehaviour {

	public AudioClip teleportSound;

	private Animator animator;

	void Start() {
		animator = GetComponent<Animator>();
	}

	void OnTriggerEnter2D(Collider2D collider) {
		if(collider.gameObject.tag == "Player") {
			StartCoroutine(Teleport());
		}
	}

	IEnumerator Teleport() {
		animator.SetTrigger("Teleport");
		AudioSource.PlayClipAtPoint(teleportSound, transform.position);
		yield return new WaitForSeconds(1.0f);
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
	}
}
