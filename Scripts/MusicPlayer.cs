﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class MusicPlayer : MonoBehaviour {
	
	static MusicPlayer instance = null;

	public AudioClip startClip;
	public AudioClip gameClip;
	public AudioClip bossClip;
	public AudioClip endClip;

	private AudioSource music;
	private string sceneName;

	void Start() {
		if (instance != null && instance != this) {
			Destroy(gameObject);
			Debug.Log("Duplicate music player found. Destroying");
		}
		else {
			instance = this;
			GameObject.DontDestroyOnLoad(gameObject);
			music = GetComponent<AudioSource>();
		}
	}

	public void ChangeMusic() {
		sceneName = SceneManager.GetActiveScene().name;
		Debug.Log("change music function: " + sceneName);

//		if(!music.isPlaying) {
//			Debug.Log("no music is playing");
//		}
//		else {
//			Debug.Log("must be music");
//		}

		if(sceneName == "_Start"
			|| sceneName == "Lose"
			|| sceneName == "Win") {
			if(music.clip != startClip) {
				Debug.Log("Playing start game music");
				music.Stop();
				music.clip = startClip;
				music.loop = true;
				music.Play();
			}
			else {
				Debug.Log("Continuing start game music");
			}
		}

//		play different music on different levels
		if(sceneName == "level-01"
			|| sceneName == "level-02"
			|| sceneName == "level-03"
			|| sceneName == "level-04"
			|| sceneName == "level-05"
			|| sceneName == "level-06"
			|| sceneName == "level-07"
			|| sceneName == "level-08"
			|| sceneName == "level-09"
			|| sceneName == "level-10") {

			if(music.clip != gameClip) {
				Debug.Log("Playing general game music");
				music.Stop();
				music.clip = gameClip;
				music.loop = true;
				music.Play();
			}
			else {
				Debug.Log("Continuing general game music");
			}
		}
		if(sceneName == "boss-01") {
			Debug.Log("Playing boss game music");
			music.Stop();
			music.clip = bossClip;
			music.loop = true;
			music.Play();
		}
	}

//	void OnLevelWasLoaded(int level) {
//		Debug.Log("music player loaded level " + level);
//
//		// play different music on different levels
//		if(level == 1) {
//			Debug.Log("Playing general game music");
//			music.Stop();
//			music.clip = gameClip;
//			music.loop = true;
//			music.Play();
//		}
//		if(level == 3 || level == 5 || level == 7 || level == 9 || level == 11 || level == 13) {
//			Debug.Log("Continuing general game music");
//			music.clip = gameClip;
//		}
//		if(level == 15) {
//			Debug.Log("Switching to boss music");
//			music.Stop();
//			music.clip = bossClip;
//			music.loop = true;
//			music.Play();
//		}
//		if(level == 16 || level == 17) {
//			Debug.Log("Switching to menu music");
//			music.Stop();
//			music.clip = endClip;
//			music.loop = true;
//			music.Play();
//		}
//	}
}
