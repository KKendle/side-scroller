﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPack : MonoBehaviour {

	private PlayerHealth playerHealth;
	private int healthPackHeal = 300;

	// Use this for initialization
	void Start () {
		playerHealth = GameObject.Find("PlayerHealth").GetComponent<PlayerHealth>();
		if(playerHealth == null) {
			Debug.Log("Player Health not found");
		}
	}
	
	void OnTriggerEnter2D(Collider2D collider) {
		if(collider.gameObject.tag == "Player") {
			Debug.Log("Player hit HealthPack");
			Debug.Log("Player health is at " + PlayerHealth.playerHealth);
			playerHealth.HealthRestore(healthPackHeal);
			Debug.Log("Player health is now at " + PlayerHealth.playerHealth);
			Collect();
		}
	}

	public void Collect() {
		Debug.Log("Picking up " + gameObject);
		Destroy(gameObject);
	}
}
