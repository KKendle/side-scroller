﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelCounter : MonoBehaviour {

	private Text levelText;

	void Start() {
		levelText = GetComponent<Text>();
		levelText.text = SceneManager.GetActiveScene().name;
		Debug.Log(levelText.text);
	}
}
