﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class Microscope : MonoBehaviour {

	public GameObject helperText;
	public GameObject helperTextUsed;
	public GameObject[] spawner;

	private bool spawnUsed = false;
	private bool forceAmoeba = false;
	private Vector3 offset;

	void OnTriggerStay2D(Collider2D collider) {
		if(collider.gameObject.tag == "Player") {
			if(spawnUsed == false) {
				helperText.gameObject.SetActive(true);
				helperTextUsed.gameObject.SetActive(false);
				if(Input.GetKeyDown(KeyCode.X)) {
					SpawnCharacterLookup();
					spawnUsed = true;
				}
			}
			if(spawnUsed == true) {
				helperText.gameObject.SetActive(false);
				helperTextUsed.gameObject.SetActive(true);
			}
		}
	}

	void OnTriggerExit2D(Collider2D collider) {
		helperText.gameObject.SetActive(false);
		helperTextUsed.gameObject.SetActive(false);
	}

	void SpawnHelper() {
		if(SceneManager.GetActiveScene().name == "level-02") {
			Debug.Log("on level two, spawn amoeba");
			forceAmoeba = true;
		}
		else if(SceneManager.GetActiveScene().name == "level-03") {
			Debug.Log("on level three, spawn amoeba");
			forceAmoeba = true;
		}
		else {
			Debug.Log("not on level one or two");
			forceAmoeba = false;
		}
	}

	void SpawnCharacterLookup() {
		SpawnHelper();
		if(forceAmoeba) {
			Debug.Log("on a level with guaranteed amoeba");
			GameObject characterLookup = spawner[0];
			Debug.Log("Spawning " + characterLookup);
			CharacterOffset(characterLookup);
		}
		else {
			GameObject characterLookup = spawner[Random.Range(0, spawner.Length)];
			Debug.Log("Spawning " + characterLookup);
			CharacterOffset(characterLookup);
		}
	}

	void CharacterOffset(GameObject characterLookup) {
		string characterName = characterLookup.transform.name;
		if(characterName == "Amoeba") {
			Debug.Log("offset for amoeba character");
			offset = transform.position + new Vector3(2f, -.9f, 0);
		}
		if(characterName == "Goo") {
			Debug.Log("offset for goo character");
			offset = transform.position + new Vector3(2f, -1.34f, 0);
		}
		if(characterName == "Eyeball") {
			Debug.Log("offset for eyeball character");
			offset = transform.position + new Vector3(2f, -.8f, 0);
		}
		SpawnCharacter(characterLookup, offset);
	}

	void SpawnCharacter(GameObject characterLookup, Vector3 offset) {
		GameObject randomCharacter = Instantiate(characterLookup, offset, Quaternion.identity) as GameObject;
		randomCharacter.transform.parent = gameObject.transform;
	}
}
