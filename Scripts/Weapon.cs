﻿using UnityEngine;
using System.Collections;

public class Weapon : MonoBehaviour {

	public int damage;

	public int GetDamage() {
		Debug.Log("Damage done " + damage);
		return damage;
	}

	public void Hit() {
		Debug.Log("Weapon Hit. Destroying weapon.");
		Destroy(gameObject);
	}
}
