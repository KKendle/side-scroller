[ExperiMENTAL Tasks]
* start screen
    * some sort of background image??
        * gif of scientist making potions
        * gif of scientist checking computer
* add other elements
    * computer
    * periodic table picture
* add score
* add load screens
* add pause screens

[Bugs]
* can throw super fast
    * holding down should still throw continuously
    * pressing attack each time should have a minimum 'reload' time
